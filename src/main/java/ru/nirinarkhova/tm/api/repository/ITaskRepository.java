package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task findOneByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId);

    void removeAllByProjectId(String projectId);

    Task bindTaskByProject(String taskId, String projectId);

    Task unbindTaskByProject(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
