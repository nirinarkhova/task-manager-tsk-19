package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}
