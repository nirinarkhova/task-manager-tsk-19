package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String login);

    User removeByLogin(final String Login );

}
