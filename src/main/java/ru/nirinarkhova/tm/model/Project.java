package ru.nirinarkhova.tm.model;

import ru.nirinarkhova.tm.api.entity.IWBS;
import ru.nirinarkhova.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractEntity implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateStart;

    private Date dateFinish;

    private Date created = new Date();

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public Date getCreated() {
        return created;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Project() {
    }

    public Project(String name) { this.name = name; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
