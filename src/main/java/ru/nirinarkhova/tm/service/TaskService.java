package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.api.service.ITaskService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, String name, String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, String name, String description) {
        if (id == null) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) {
        if (id == null) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(String name) {
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(String id) {
        if (id == null) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) {
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        if (id == null) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String name, Status status) {
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
