package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.repository.IUserRepository;
import ru.nirinarkhova.tm.api.service.IUserService;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.exception.empty.*;
import ru.nirinarkhova.tm.exception.system.EmailExistsException;
import ru.nirinarkhova.tm.exception.system.LoginExistsException;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);

    }

    @Override
    public boolean isLoginExist(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return findByLogin(login) != null;

    }

    @Override
    public boolean isEmailExist(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return findByEmail(email) != null;

    }

    @Override
    public User updateUser(String userId, String firstName, String lastName, String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User removeByLogin(final String Login) {
        if (Login == null || Login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeById(Login);
    }

    @Override
    public  User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (isEmailExist(email)) throw new EmailExistsException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(String userId, String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

}
