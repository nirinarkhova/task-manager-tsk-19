package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.repository.IProjectRepository;
import ru.nirinarkhova.tm.api.service.IProjectTaskService;
import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProject(String taskId, String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.bindTaskByProject(taskId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.unbindTaskByProject(taskId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}
