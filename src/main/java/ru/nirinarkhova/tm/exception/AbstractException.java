package ru.nirinarkhova.tm.exception;

public abstract class AbstractException extends RuntimeException {

    protected String message;

    protected AbstractException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}

