package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(entities);
        tasks.sort(comparator);
        return tasks;
    }

    public Task findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task: entities) {
            if(name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task findOneByProjectId(String projectId) {
        for (final Task task: entities) {
            if(projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public Task bindTaskByProject(String taskId, String projectId) {
        if (projectId == null) return null;
        if (taskId == null) return null;
        final Task task = findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task: entities){
            if (projectId.equals(task.getProjectId())) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(String projectId) {
        entities.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task unbindTaskByProject(String taskId) {
        if (taskId == null) return null;
        final Task task = findById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}

