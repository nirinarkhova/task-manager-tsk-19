package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.IUserRepository;
import ru.nirinarkhova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

}

