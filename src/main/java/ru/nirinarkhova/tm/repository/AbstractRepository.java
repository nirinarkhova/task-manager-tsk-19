package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        for (final E entity: entities) {
            if (entity == null) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) throw new ObjectNotFoundException();
        entities.remove(entity);
        return entity;
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

}

