package ru.nirinarkhova.tm.command.task;

import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "change task status to Complete by task name.";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER TASK NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}

