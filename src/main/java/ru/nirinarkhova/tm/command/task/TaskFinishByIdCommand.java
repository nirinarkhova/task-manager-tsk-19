package ru.nirinarkhova.tm.command.task;

import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String description() {
        return "change task status to Complete by task id.";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER TASK ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

}

