package ru.nirinarkhova.tm.command.task;

import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    public String arg() {
        return null;
    }

    public String name() {
        return "task-remove-by-name";
    }

    public String description() {
        return "delete a task by name.";
    }

    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER TASK NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeOneByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}

