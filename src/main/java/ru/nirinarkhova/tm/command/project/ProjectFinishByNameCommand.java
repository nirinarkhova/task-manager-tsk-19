package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String description() {
        return "change project status to Complete by project name.";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER PROJECT NAME:]");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

}

