package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "update a project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[ENTER PROJECT NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = serviceLocator.getProjectService().updateProjectByIndex(index, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
    }

}

