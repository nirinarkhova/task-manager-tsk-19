package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @Override
    public String description() {
        return "change project status by project name.";
    }

    @Override
    public void execute() {
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = serviceLocator.getProjectService().findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        final Project projectStatusUpdate = serviceLocator.getProjectService().changeProjectStatusByName(name, status);
        if (projectStatusUpdate == null) throw new ProjectNotFoundException();
    }

}

