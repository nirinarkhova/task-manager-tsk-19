package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "delete a project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER PROJECT INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().removeOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

}

