package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER PROJECT NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().add(name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

}

