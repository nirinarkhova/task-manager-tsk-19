package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectRemoveByIdWithTasksCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id-with-tasks";
    }

    @Override
    public String description() {
        return "delete project with all its tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectTaskService().removeProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}

