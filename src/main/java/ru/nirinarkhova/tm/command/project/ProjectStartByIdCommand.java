package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String description() {
        return "change project status to In progress by project id.";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

}

