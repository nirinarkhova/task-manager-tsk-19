package ru.nirinarkhova.tm.command;

import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Start Date: " + project.getDateStart());
        System.out.println("Finish Date: " + project.getDateFinish());
        System.out.println("Created: " + project.getCreated());
    }

}

