package ru.nirinarkhova.tm.command.authorization;

import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.model.User;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "show your profile information.";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

}

