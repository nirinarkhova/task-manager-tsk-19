package ru.nirinarkhova.tm.command.authorization;

import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.exception.entity.UserNotFoundException;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "add your name to your profile.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("[ENTER FIRST NAME:]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        final String middleName = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        if (user == null) throw new UserNotFoundException();
    }

}

