package ru.nirinarkhova.tm.bootstrap;

import ru.nirinarkhova.tm.api.entity.ILoggerService;
import ru.nirinarkhova.tm.api.repository.*;
import ru.nirinarkhova.tm.api.service.*;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.command.authorization.*;
import ru.nirinarkhova.tm.command.project.*;
import ru.nirinarkhova.tm.command.system.*;
import ru.nirinarkhova.tm.command.task.*;
import ru.nirinarkhova.tm.controller.SystemInfoCommand;
import ru.nirinarkhova.tm.exception.system.UnknownArgumentException;
import ru.nirinarkhova.tm.exception.system.UnknownCommandException;
import ru.nirinarkhova.tm.repository.CommandRepository;
import ru.nirinarkhova.tm.repository.ProjectRepository;
import ru.nirinarkhova.tm.repository.TaskRepository;
import ru.nirinarkhova.tm.repository.UserRepository;
import ru.nirinarkhova.tm.service.*;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private  final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private  final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private  final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new AboutCommand());
        registry(new ArgumentsListCommand());
        registry(new CommandsListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());

        registry(new UserPasswordChangeCommand());
        registry(new UserLogInCommand());
        registry(new UserLogOutCommand());
        registry(new UserRegisterCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIdWithTasksCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowAllCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowAllByProjectIdCommand());
        registry(new TaskShowAllCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    public void run(final String... args) throws UnknownArgumentException {
        loggerService.info("***WELCOME TO TASK MANAGER***");
        if (parseArgs(args)) System.exit(0);
        while (true){
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }


    public boolean parseArgs(String[] args) throws UnknownArgumentException {
        if(args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public IUserService getUserService() {
        return userService;
    }

}
